import * as http from 'node:http';
import { html } from './getHtml.js';
import { json } from './getJson.js';
import { uuid } from './getUuid.js';
import { status } from './getStatus.js';

const server = http.createServer((req, res) => {
  if (req.url === '/html') {
    res.write(html);
    res.end();
  }
  if (req.url === '/json') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(json));
    res.end();
  }
  if (req.url === '/uuid') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(uuid));
    res.end();
  }
  if (req.url === '/status/100') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(status['100']));
    res.end();
  }
  if (req.url === '/status/200') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(status['200']));
    res.end();
  }
  if (req.url === '/status/300') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(status['300']));
    res.end();
  }
  if (req.url === '/status/400') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(status['400']));
    res.end();
  }
  if (req.url === '/status/500') {
    res.setHeader('Content-Type', 'application/json');
    res.write(JSON.stringify(status['500']));
    res.end();
  }
  let delay = req.url.split('/');
  if (delay[1] === 'delay') {
    setTimeout(
      () => {
        res.write(JSON.stringify(status['200']));
        res.end();
      },
      Number(delay[2] * 1000),
    );
  }
});

server.listen(3000);
