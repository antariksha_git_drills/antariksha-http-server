export const status = {
  100: 'Continue',
  200: 'OK',
  300: 'Multiple Choices',
  400: 'Bad Request',
  500: 'Internal Server Error',
};
